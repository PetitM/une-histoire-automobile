import unittest
from ModifierFormat import changerDelimiteur, changerNomCol, changerOrdreCol, recupCSV

ligne_test = "adress|carrosserie|categorie|couleur|cylindree|date_immat|denomination|energy|firstname|immat|marque|name|places|poids|puissance|type_variante_version|vin"
nouvelle_ligne = "adresse_titulaire|carrosserie|categorie|couleur|cylindree|date_immatriculation|denomination_commerciale|energie|prenom|immatriculation|marque|nom|places|poids|puissance|type_variante_version|vin"

class TestModifierFormatFunctions(unnitest.TestCase):

    def test_changerDelimiteur(sefl):
        self.assertEqual(changerDelimiteur("ref.|name|price"), "ref.;name;price")

    
    def test_changerNomCol(self):
        self.assertEqual(changerNomCol(ligne_test, nouvelle_ligne), nouvelle_ligne)



    